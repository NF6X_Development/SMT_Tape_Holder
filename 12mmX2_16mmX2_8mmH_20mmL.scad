// Parametric SMT Tape Holder Generator for OpenSCAD
// Copyright (C) 2017 Mark J. Blair <nf6x@nf6x.net>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use <SMT_Tape_Holder.scad>

// 2 each 12mm channels, 2 each 16mm channels
// 8mm tape thickness
// 20mm long window between rails
holder([12, 12, 16, 16], 8, 20);


