// Parametric SMT Tape Holder Generator for OpenSCAD
// Copyright (C) 2017 Mark J. Blair <nf6x@nf6x.net>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// All units in mm
C  = 0.5;       // Clearance added to tape width
T  = 1;         // Tape edge thickness
E1 = 2.75;      // Width of sprocket hole edge support
E2 = 0.75;      // Width of non sprocket hole edge support
S  = 2;         // Width of sides of each channel
B  = 1;         // Base thickness, rail thickness, etc.
R  = 3;         // Length of hold-down rails
Ov = 0.01;      // Overlap between adjacent channels:
                // Negative disconnects channels from each other.
                // Positive connects with overlap.
                // Zero may produce non-manifold geometry due to
                // rounding errors (?).

// Select elements from a vector
function select(vector,indices) = [ for (index = indices) vector[index] ];

// Render single channel of holder
//   Tape assumed to feed left (-X) to right (+X),
//   with sprocket holes at top (+Y).
//   W = width of tape for this channel.
//   K = tape overall thickness.
//   L = Length of window between rails
//   Offset = horizontal offset of this channel.
module channel(W, K, L, Offset=0) {
    // Offset successive holders along Y axis
    translate([0, Offset, 0]) {

        // Main body of holder
        rotate([90, 0, 90])
            linear_extrude(height = L + 3*R)
                polygon(points = [[-Ov, 0], [-Ov, K+B], [S-C/2, K+B],
                    [S-C/2, K+B-T], [S+E2, K+B-T], [S+E2, B],
                    [S+W-E1, B], [S+W-E1, K+B-T], [S+W+C/2, K+B-T],
                    [S+W+C/2, K+B], [W+Ov+2*S, K+B], [W+Ov+2*S, 0]]);

        // Left hold-down rail with cover tape fold-back buckle
        translate([0, 0, K+B])
            linear_extrude(height = B)
                polygon(points = [[0, -Ov], [0, W+Ov+2*S],
                    [2*R, W+Ov+2*S], [2*R, -Ov]]);
        translate([0, 0, K+2*B])
            linear_extrude(height = T)
                polygon(points = [[0, -Ov], [0, S-C/2],
                    [R, S-C/2], [R, -Ov]]);
        translate([0, 0, K+2*B])
            linear_extrude(height = T)
                polygon(points = [[0, S+W+C/2], [0, W+Ov+2*S],
                    [R, W+Ov+2*S], [R, S+W+C/2]]);
        translate([0, 0, K+2*B+T])
            linear_extrude(height = B)
                polygon(points = [[0, -Ov], [0, W+Ov+2*S],
                    [R, W+Ov+2*S], [R, -Ov]]);

        // Right hold-down rail
        translate([0, 0, K+B])
            linear_extrude(height = B)
                polygon(points = [[L+2*R, -Ov], [L+2*R, W+Ov+2*S],
                    [L+3*R, W+Ov+2*S], [L+3*R, -Ov]]);
    }
}

// Recursively render channels of holder
//   Widths:    List of tape widths per channel.
//   Thickness: Overall thickness of tapes.
//   Length:    Length of window between hold-down rails.
//              Window length is specified instead of overall holder
//              length to make it easier to specify a holder which
//              exposes a specific number of components between the
//              hold-down rails.
//   Offset:    Horizontal offset. Leave this zero in user code;
//              this argument is used by recursive calls of this function.
module holder(Widths=[8], Thickness=3, Length=20, Offset=0) {
    union() {
        channel(Widths[0], Thickness, Length, Offset);
        if (len(Widths) > 1) {
            NewWidths = select(Widths, [1:len(Widths)-1]);
            holder(NewWidths, Thickness, Length, Offset + Widths[0] + 2*S);
        }
    }
}

// Example:
//   Channel widths: 8mm, 8mm, 12mm, 16mm
//   Tape thickness: 3mm
//   Window length:  20mm
holder([8, 8, 12, 16], 3, 20);
